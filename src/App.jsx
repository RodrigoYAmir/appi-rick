import { ComponenteDos } from "./Componentes/ComponenteDos"
import ComponenteUno from "./Componentes/ComponenteUno"

function App() {
  

  return (
    <div className="caja">
    
      <ComponenteUno/>
      <ComponenteDos/>
    </div>
  )
}

export default App
